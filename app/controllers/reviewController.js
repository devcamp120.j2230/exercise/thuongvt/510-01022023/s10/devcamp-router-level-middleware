// khai báo thư viện mongoose
const mongoose = require("mongoose");
// Khai báo Model
const reviewModel =  require("../models/reviewModel");
const courseModel = require("../models/courseModel");

const createReviewOfCourse = (req,res)=>{
    //B1 chuẩn bị dữ liệu
    let Id = req.params.CourseId;
    let body = req.body;
    // B2 Kiểm tra dữ liệu 
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "Course ID is not valid!"
        })
    }

    // Star là số nguyên, và lớn hơn 0 nhỏ hơn hoặc bằng 5
    if(body.stars === undefined || !(Number.isInteger(body.stars) && body.stars > 0 && body.stars <= 5)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "Star is not valid!"
        })
    }

    //B3 thao tác với cơ sở dữ liệu
    let newReviewData = {
        _id: mongoose.Types.ObjectId(),
        stars: body.stars,
        note: body.note
    }
    reviewModel.create(newReviewData,(err,data)=>{
        if(err) {
            return res.status(500).json({
                status: "Internal server error2",
                message: err.message
            })
        }

        // Khi tạo review xong cần thêm id review mới vào mảng reviews của course
        courseModel.findByIdAndUpdate(Id,
            {$push:{reviews: data._id}},
            (err1,data1)=>{
                if(err1){
                    return res.status(500).json({
                        status: "Internal server error1",
                        message: err1.message
                    })
                }
                return res.status(201).json({
                    status: "Create review successfully",
                    data: data
                })
            }
        )
    })
}

// Get review by Id
const getReviewById = (req,res)=>{
    //B1 chuẩn bị dữ liệu
    let Id = req.params.ReviewId
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "ReviewID không hợp lệ"
        })
    }
    //B3 Gọi model thực hiện nhiệm vụ
    reviewModel.findById(Id,(err,data)=>{
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        if(data){
            return res.status(200).json({
                status: "Get detail review successfully",
                data: data
            })
        } 
        else {
            return response.status(404).json({
                status: "Not Found"
            })
        }
    })
}

// Lấy toàn bộ dữ liệu review
const getAllReview = (req,res)=>{
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    reviewModel.find((err,data)=>{
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        return res.status(200).json({
            status: "Get all review successfully",
            data: data
        })
    })
}

//Lấy toàn bộ review của một course
const getAllReviewOfCourse = (req,res)=>{
    // B1: Chuẩn bị dữ liệu
    let Id = req.params.CourseId
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "Course ID is not valid!"
        })
    }
    // B3: Gọi courseModel tạo dữ liệu
    courseModel
    .findById(Id)
    .populate("reviews")
    .exec((err,data)=>{
        if(err) {
            return response.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        
        return res.status(200).json({
            status: "Get reviews successfully",
            data: data.reviews
        })
    })
}
// update review by id
const updateReviewById = (req,res)=>{
    // B1: Chuẩn bị dữ liệu
    let Id = req.params.ReviewId
    let body = req.body
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "Course ID is not valid!"
        })
    }
    // Star là số nguyên, và lớn hơn 0 nhỏ hơn hoặc bằng 5
    if(body.stars === undefined || !(Number.isInteger(body.stars) && body.stars > 0 && body.stars <= 5)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "Star is not valid!"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    //tạo ra một biến hấng dữ liệu
    let newUpdateData = {
        stars: body.stars,
        note: body.note
    }
    //Nếu dữ liệu đưa vào khác rỗng thì chấp nhận còn không thì không chấp nhận
    // if(newUpdateData.stars !== undefined) {
    //     newUpdateData.stars = body.stars
    // }
    // if(newUpdateData.note !== undefined) {
    //     newUpdateData.note = body.note
    // }

    reviewModel.findByIdAndUpdate(Id,newUpdateData,(err,data)=>{
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        if(data) {
            return res.status(200).json({
                status: "Update reivew successfully",
                data: data
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
    })
}

// xóa review của một course 
const deleteReviewById = (req,res)=>{
    // B1: Chuẩn bị dữ liệu
    let RId = req.params.ReviewId;
    let CId = req.params.CourseId;
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(RId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "Review ID is not valid!"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(CId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "Course ID is not valid!"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    reviewModel.findByIdAndDelete(RId,(err,data)=>{
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        courseModel.findByIdAndUpdate(CId,
            {$pull:{reviews: RId}},
            (err1,data1)=>{
                if(err1) {
                    return res.status(500).json({
                        status: "Internal server error",
                        message: err1.message
                    })
                }
                else{
                    return res.status(201).json({
                        status: "delete review successfully",
                        data: data
                    })
                }
            })

    })
}
//exports thành module
module.exports = {
    createReviewOfCourse,
    getReviewById,
    getAllReview,
    getAllReviewOfCourse,
    updateReviewById,
    deleteReviewById
}