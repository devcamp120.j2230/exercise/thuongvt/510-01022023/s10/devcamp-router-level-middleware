//Khai báo thư viện express
const express = require("express");

//Khai báo middleware
const {
    getAllReviewMidlleware,
    getReviewMidlleware,
    postReviewMidlleware,
    putReviewMidlleware,
    deleteReviewMidlleware
} = require (`../middleware/reviewMiddleware`);

// Khai báo controller
const {
    createReviewOfCourse,
    getReviewById,
    getAllReview,
    getAllReviewOfCourse,
    updateReviewById,
    deleteReviewById
} = require ("../controllers/reviewController")

// Tạo router
const reviewRouter = express.Router();

//Sử dụng Router
// Lấy toàn bộ review
reviewRouter.get("/Review", getAllReviewMidlleware, getAllReview)
// createReviewOfCourse
reviewRouter.post("/Courses/:CourseId/Reviews", postReviewMidlleware, createReviewOfCourse );
// getAllReviewOfCourse
reviewRouter.get("/Courses/:CourseId/Reviews", getAllReviewMidlleware, getAllReviewOfCourse);
// get Review by Id
reviewRouter.get("/Review/:ReviewId", getReviewMidlleware, getReviewById);

reviewRouter.put("/Review/:ReviewId", putReviewMidlleware,updateReviewById );

reviewRouter.delete("/Courses/:CourseId/Reviews/:ReviewId",deleteReviewMidlleware, deleteReviewById);

module.exports = {reviewRouter}